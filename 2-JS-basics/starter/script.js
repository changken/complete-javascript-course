/******************************
 * Variable and data types
 */
/*
var firstName = 'John';
console.log(firstName);

var lastName = 'Smith';
var age = 28;

var fullAge = true;
console.log(fullAge);

var job;//undefined
console.log(job);

job = 'Teacher';
console.log(job);

//1. camel case => javascipt
//2. variable can't start at number or symbol(except for $, _)
//3. can't use the javascipt preserve word

// Variable naming rules
var _3year = 3;
var johnMark = 'John and MArk';
var if = 23;
*/

/******************************
 * Variable and mutation and type coercion
 */

/*
var firstName = "John";
var age = 28;

// 強制轉型 Type coercion
console.log(firstName + ' ' + age);

// 宣告多個變數
var job, isMarried;
job = 'Teacher';
isMarried = false;

console.log(firstName + ' is a ' + age + ' year old ' + job + '. Is he married? ' + isMarried );

// Variable muation 自動轉型
age = 'twenty eight';
job = 'driver';

alert(firstName + ' is a ' + age + ' year old ' + job + '. Is he married? ' + isMarried );

var lastName = prompt('What is his last Name?');
console.log(firstName + ' ' + lastName);
*/

/******************************
 * Basic Operator
 */

/*
// Math operators
var year, yearJohn, yearMark;
var now = 2018;
var ageJohn = 28;
var ageMark = 33;
yearJohn = now - ageJohn;
yearMark = now - ageMark;

console.log(yearJohn);

console.log(now + 2);
console.log(now * 2);
console.log(now / 10);

// Logical operators
var johnOlder = ageJohn < ageMark;
console.log(johnOlder);

// typeof operator
console.log(typeof johnOlder);
console.log(typeof ageJohn);
console.log(typeof 'Mark is older than John');
var x;
console.log(typeof x);
*/

/******************************
 * Operator precedence
 */

/*
var now = 2018;
var yearJohn = 1989;
var fullAge = 18;

// Multiple operator
var isFullAge = now - yearJohn >= fullAge; // true
console.log(isFullAge);

// Grouping
var ageJohn = now - yearJohn;
var ageMark = 35;
var average = (ageJohn + ageMark) / 2;
console.log(average);

// Mutiple assignments
var x, y;
// because = is right-to-left
x = y = (3 + 5) * 4 - 6; // 8 * 4 - 6 //32-6 //26
console.log(x, y);

// More  operators
x *= 2;
console.log(x);
x += 10;
console.log(x);
x--;
console.log(x);
*/

/******************************
 * CODING CHALLENGE
 * BMI
 */

/*
var massMark, massJohn, heightMark, heightJohn, BMIMark, BMIJohn;

massMark = 78; //kg
massJohn = 92;
heightMark = 1.69;//meters
heightJohn = 1.95;

BMIMark = massMark / (heightMark * heightMark);
BMIJohn = massJohn / (heightJohn * heightJohn);

var markHigherBMI = BMIMark > BMIJohn;

console.log(BMIMark, BMIJohn);
// escaping
console.log('Is Mark\'s BMI higher than John\'s? ' + markHigherBMI);
*/

/******************************
 * If / else statement
 */

/*
var firstName = 'John';
var civilStatus = 'single';

if(civilStatus === 'married') {
    console.log(firstName + ' is married!');
} else {
    console.log(firstName + ' will hopefully marry sonn :)');
}

var isMarried = true;

if(isMarried) {
    console.log(firstName + ' is married!');
} else {
    console.log(firstName + ' will hopefully marry sonn :)');
}

var massMark, massJohn, heightMark, heightJohn, BMIMark, BMIJohn;

massMark = 78; //kg
massJohn = 92;
heightMark = 1.69;//meters
heightJohn = 1.95;

BMIMark = massMark / (heightMark * heightMark);
BMIJohn = massJohn / (heightJohn * heightJohn);

if(BMIMark > BMIJohn) {
    console.log('Mark\'s BMI is higher than John\'s.');
} else {
    console.log('John\'s BMI is higher than Mark\'s.');
}
*/

/******************************
 * Boolean logic
 */

/*
var firstName = 'John';
var age = 20;

if(age < 13){
    console.log(firstName + ' is a boy.');
} else if(age >= 13 && age < 20) { //Between 13 and 20 = age >= 13 AND age < 20
    console.log(firstName + ' is a teenager.');   
} else if (age >= 20 && age < 30){
    console.log(firstName + ' is a young man.');   
}else {
    console.log(firstName + ' is a man.');
}
*/

/******************************
 * The Ternary Operator and Switch Statement
 */

/*
var firstName = 'John';
var age = 14;

// Ternary Operator
age >= 18 ? console.log(firstName + ' drinks beer.') : console.log(firstName + ' drinks juice.');

var drink = age >= 18 ? 'beer' : 'juice';
console.log(drink);

//if(age >= 18){
//    var drink = 'beer';
//} else {
//    var drink = 'juice';
//}

// Switch Statement
var job = 'instructor';

switch(job){
    case 'teacher':
    case 'instructor':
        console.log(firstName + ' teaches kids how to code.');
        break;
    case 'driver':
        console.log(firstName + ' drives an uber in Lisbon.');
        break;
    case 'designer':
        console.log(firstName + ' design beautiful websites.');
        break;
    default:
        console.log(firstName + ' does something else.');
}

age = 22;

switch(true){
    case age < 13:
        console.log(firstName + ' is a boy.');
        break;
    case age >= 13 && age < 20:
        console.log(firstName + ' is a teenager.');   
        break;
    case age >= 20 && age < 30:
        console.log(firstName + ' is a young man.'); 
        break;
    default:
        console.log(firstName + ' is a man.');
}
*/


/******************************
 * Truthy and Falsy value and equality operators
 */
 
// falsy values: undefined, null, 0, '', NaN
// truthy values: NOT falsy values

/*
var height;

height = 23;

if(height || height === 0){
    console.log('Variable is defined!');
} else {
    console.log('Variable has NOT been defined!');
}

// Equality operators
// 非嚴格型態全等的==
if(height == '23'){
    console.log('The == operator does type coercion!');
}
*/

/******************************
 * CODING CHALLENGE 2
 */

/*
var scoreJohn = (89 + 120 + 103) / 3;
var scoreMike = (129 + 94 + 123) / 3;
var scoreMary = (97 + 134 + 105) / 3;
console.log(scoreJohn, scoreMike, scoreMary);

if(scoreJohn > scoreMike && scoreJohn > scoreMary) {
    console.log('John\'s team wins with ' + scoreJohn + ' points');
} else if(scoreMike > scoreJohn && scoreMike > scoreMary) {
    console.log('Mike\'s team wins with ' + scoreMike + ' points');
}else if(scoreMary > scoreJohn && scoreMary > scoreMike){
    console.log('Mary\'s team wins with ' + scoreMary + ' points');
} else {
    console.log('There is a draw');
}

// my version
var c = scoreJohn;
if(c < scoreMike){
    c = scoreMike;
}
if(c < scoreMary){
    c = scoreMary;
}
console.log(c);
*/

/*if(scoreJohn > scoreMike){
    console.log('John\'s team wins with ' + scoreJohn + ' points');
} else if(scoreMike > scoreJohn) {
    console.log('Mike\'s team wins with ' + scoreMike + ' points');
} else { //draw
    console.log('There is a draw');
}*/

/******************************
 * Functions
 */

/*
function calculateAge(birthYear) {
    return 2018 - birthYear;
}

var ageJohn = calculateAge(1990);
var ageMike = calculateAge(1948);
var ageJane = calculateAge(1969);

console.log(ageJohn, ageMike, ageJane);

function yearsUntilRetirement(year, firstName){
    var age = calculateAge(year);
    var retirement = 65 - age;
    
    if(retirement > 0){
        console.log(firstName + ' retires in ' + retirement + ' years.');   
    } else {
        console.log(firstName + ' is already retired.');
    }
}

yearsUntilRetirement(1990, 'John');
yearsUntilRetirement(1948, 'Mike');
yearsUntilRetirement(1969, 'Jane');
*/

/******************************
 * Function Statements and Expressions
 */

/*
// function declaration
// function whatDoYouDo(job, firstName) {}

// function expression
var whatDoYouDo = function(job, firstName) {
    switch(job){
        case 'teacher':
            return firstName + ' teaches kids how to code';
        case 'driver':
            return firstName + ' drives a cab in Lisbon.';
        case 'designer':
            return firstName + ' designs a beautiful websites.';
        default:
            return firstName + ' does something else.';
    }
};

console.log(whatDoYouDo('teacher', 'John'));
console.log(whatDoYouDo('designer', 'Jane'));
console.log(whatDoYouDo('retired', 'Mark'));
*/

/******************************
 * Arrays
 */

/*
// Initialize new array
var names = ['John', 'Mark', 'Jane'];
var years = new Array(1990, 1969, 1948);

console.log(names[2]);
console.log(names.length);

// Mutate array data
names[1] = 'Ben';
names[names.length] = 'Mary';
console.log(names);

// Different data types
var john = ['John', 'Smith', 1990, 'designer', false];

john.push('blue');//append
john.unshift('Mr.'); // add to the begin
console.log(john);

john.pop();
john.pop();
john.shift();
console.log(john);

console.log(john.indexOf(23));

var isDesginer = john.indexOf('designer') === -1 ? 'John is NOT a designer' : 'John IS a designer.';

console.log(isDesginer);
*/

/******************************
 * CODING CHALLENGE 2
 */

/*
function tipCalculator(bill){
    var percentage;
    if(bill < 50){
        percentage = 0.2;
    } else if(bill >= 50 && bill < 200){
        percentage = 0.15;
    } else {
        percentage = 0.1;
    }
    
    return percentage * bill;
}

console.log(tipCalculator(10));

var bills = [124, 48, 268];
var tips = [tipCalculator(bills[0]), tipCalculator(bills[1]), tipCalculator(bills[2])];
var finalValues = [bills[0] + tips[0],
                  bills[1] + tips[1],
                  bills[2] + tips[2]];

console.log(tips, finalValues);
*/

/******************************
 * Objects and properties
 */

/*
// Object literal
var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthYear: 1990,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false
};

console.log(john.firstName);
console.log(john['lastName']);

var x = 'birthYear';
console.log(john[x]);

john.job = 'designer';
john['isMarried'] = true;
console.log(john);

// new Object syntax
var jane = new Object();
jane.firstName = 'Jane';
jane.birthYear = 1969;
jane['lastName'] = 'Smith';

console.log(jane);
*/

/*********************************
 * Objects and methods
 */

/*var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthYear: 1992,
    fmaily: ['jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false,
    calcAge: function(){
        this.age = 2018 - this.birthYear;
    }
};

john.calcAge();
console.log(john);*/

/*********************************
 * CODING CHALLENGE 4
 */

/*
var BMIJohn = {
    fullName: 'John Smith',
    mass: 110,
    height: 1.95,
    calcBMI: function(){
        this.bmi = this.mass / (this.height * this.height);
        
        return this.bmi;
    }
};

var BMIMark = {
    fullName: 'Mark Miller',
    mass: 78,
    height: 1.69,
    calcBMI: function(){
        this.bmi = this.mass / (this.height * this.height);
        
        return this.bmi;
    }
};

if(BMIJohn.calcBMI() > BMIMark.calcBMI()){
    console.log(BMIJohn.fullName + ' has a higher BMI of ' + BMIJohn.bmi);
} else if(BMIJohn.bmi < BMIMark.bmi){
    console.log(BMIMark.fullName + ' has a higher BMI of ' + BMIMark.bmi);
} else {
    console.log('They have same BMI!');
}
*/

/*********************************
 * Loops and iteration
 */

/*
for(var i=1; i <= 20; i+=2) {
    console.log(i);
}

var john = ['John', 'Smith', 1990, 'designer', false, 'blue'];

// for loop
for(var i=0; i< john.length; i++){
    console.log(john[i]);
}

// while loop
var i=0;
while(i< john.length) {
    console.log(john[i]);
    i++;
}
*/

/*
// continue and break statement
var john = ['John', 'Smith', 1990, 'designer', false, 'blue'];

for(var i=0; i< john.length; i++){
    if(typeof john[i] !== 'string') continue;
    console.log(john[i]);
}

for(var i=0; i< john.length; i++){
    if(typeof john[i] !== 'string') break;
    console.log(john[i]);
}

// looping backwards
for(var i = john.length - 1; i >= 0; i--){
    console.log(john[i]);
}
*/

/******************************
 * CODING CHALLENGE 5
 */

var john = {
    fullName: 'John Smith',
    bills: [124, 48, 268, 180, 42],
    calcTips: function(){
        this.tips = [];
        this.finalValues = [];
        
        for(var i=0; i<this.bills.length; i++){
            // Determine percentage on tipping rules
            var tipRate;
            var bill = this.bills[i];
            if(bill < 50){
                tipRate = 0.2;
            } else if(bill < 200) {
                tipRate = 0.15;
            } else {
                tipRate = 0.1;
            }
            
            // add results to corresponing arrays
            this.tips[i] = bill * tipRate;
            this.finalValues[i] = bill + bill * tipRate;
        }
    }
};

var mark = {
    fullName: 'Mark Miller',
    bills: [77, 5, 110, 45],
    calcTips: function(){
        this.tips = [];
        this.finalValues = [];
        
        for(var i=0; i<this.bills.length; i++){
            // Determine percentage on tipping rules
            var tipRate;
            var bill = this.bills[i];
            if(bill < 100){
                tipRate = 0.2;
            } else if(bill < 300) {
                tipRate = 0.1;
            } else {
                tipRate = 0.25;
            }
            
            // add results to corresponing arrays
            this.tips[i] = bill * tipRate;
            this.finalValues[i] = bill + bill * tipRate;
        }
    }
};

var calcAverage = function(tips){
    var sum = 0;
    for(var i=0; i<tips.length; i++){
        sum += tips[i];
    }
    return sum / tips.length;
};

// DO the calculations
john.calcTips();
mark.calcTips();

john.average = calcAverage(john.tips);
mark.average = calcAverage(mark.tips);

console.log(john, mark);

if(john.average > mark.average){
    console.log(john.fullName + '\'s family pays highest tips, with an average of $' + john.average);
} else if(mark.average > john.average){
    console.log(mark.fullName + '\'s family pays highest tips, with an average of $' + mark.average);
} else {
    console.log('There is a draw!');
}
