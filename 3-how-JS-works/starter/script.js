///////////////////////////////////////
// Lecture: Hoisting

// frunctions
calculateAge(1969);

function calculateAge(year){
    console.log(2016 - year);
}

// retirement(1956);
var retirement = function(year){
    console.log(65 - (2018 - year));
};

// variable

console.log(age);
var age = 23;

function foo(){
    console.log(age);
    var age = 65;
    console.log(age);
}

foo();
console.log(age);


///////////////////////////////////////
// Lecture: Scoping


// First scoping example

/*
var a = 'Hello!';
first();

function first() {
    var b = 'Hi!';
    second();

    function second() {
        var c = 'Hey!';
        console.log(a + b + c);
    }
}
*/



// Example to show the differece between execution stack and scope chain

/*
var a = 'Hello!';
first();

function first() {
    var b = 'Hi!';
    second();

    function second() {
        var c = 'Hey!';
        third()
    }
}

function third() {
    var d = 'John';
    console.log(a + b + c + d);
}
*/



///////////////////////////////////////
// Lecture: The this keyword

//console.log(this);

/*
calculateAge(1985);

function calculateAge(year){
    console.log(2016 - year);
    console.log(this);
}
*/

var john = {
    name: 'John',
    yearOfBirth: 1990,
    calculateAge: function(){
        // this關鍵字在方法呼叫時才會變成當前的物件
        console.log(this);
        console.log(2016 - this.yearOfBirth);
        
        // 當普通的函數被呼叫時 this關鍵字為window!
        /*
        function innerFunction(){
            console.log(this);
        }
        innerFunction();
        */
    }
};

john.calculateAge();

var mike = {
    name: 'Mike',
    yearOfBirth: 1984,
};

// 方法借用!
mike.calculateAge = john.calculateAge;
mike.calculateAge();
